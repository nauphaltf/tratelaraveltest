<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'Admin', 'slug' => 'admin', 'description' => 'Admin role'],
            ['name' => 'Dealer', 'slug' => 'dealer', 'description' => 'Dealer role'],
        ]);

        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'password' => bcrypt('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Dealer',
                'email' => 'dealer@example.com',
                'password' => bcrypt('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);

        $admin = User::where('email', 'admin@example.com')->first();
        $dealer = User::where('email', 'dealer@example.com')->first();
        $adminRole = Role::where('slug', 'admin')->first();
        $dealerRole = Role::where('slug', 'dealer')->first();

        $admin->roles()->sync($adminRole);
        $dealer->roles()->sync($dealerRole);
    }
}
