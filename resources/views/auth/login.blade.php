@extends('layouts.main')

@section('content')
<div class="login-form">
    <form action="{{ route('login') }}" method="post">
        @csrf
        <h2 class="text-center">Log in</h2>
        @if($errors->any())
        <div class="error">{{$errors->first()}}</div>
        @endif
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
    </form>
</div>
@endsection