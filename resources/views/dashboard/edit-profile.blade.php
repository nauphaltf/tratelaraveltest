@extends('layouts.main')

@section('content')
<div class="container">
<form  class="form-horizontal" method="post" enctype="multipart/form-data">
    @csrf
    @if ($errors->any())
         @foreach ($errors->all() as $error)
             <div class="error">{{$error}}</div>
         @endforeach
     @endif
    <div id="div_id_username" class="form-group required">
        <label for="id_username" class="control-label col-md-4  requiredField"> Name<span class="asteriskField">*</span> </label>
        <div class="controls col-md-8 ">
            <input class="input-md  textinput textInput form-control" id="name" maxlength="30" name="name" placeholder="Name" value="{{ $user->name }}" type="text" />
        </div>
    </div>
    <div class="form-group required">
        <label for="id_username" class="control-label col-md-4  requiredField">
            Email : <strong>{{ $user->email }}</strong>
        </label>
    </div>
    <div class="form-group required">
        <label for="id_username" class="control-label col-md-4  requiredField">
            Roles : <strong>{{ $role }}</strong>
        </label>
    </div>
    <div class="form-group required">
        <label for="id_email" class="control-label col-md-4  requiredField"> Profile Pic<span class="asteriskField">*</span> </label>
        <div class="controls col-md-8 ">
            <img src="/images/profile/{{ $thumbnail }}" height="200" width="200">
            <input class="input-md emailinput " id="thumbnail" name="thumbnail" placeholder="Your current email address" style="margin-bottom: 10px" type="file" />
        </div>
    </div>
    <div class="form-group required">
        <input type="submit" name="Save" class="btn btn-success">
    </div>
</form>
</div>
@endsection