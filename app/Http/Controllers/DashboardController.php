<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Requests\EditProfileRequest;

class DashboardController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepositoy)
    {
        $this->userRepository = $userRepositoy;
    }

    public function index()
    {
        return view('dashboard.index');
    }

    public function editProfile()
    {
        $user = $this->userRepository->getUser();
        $thumbnailPath = public_path().'/images/profile/' . $user->thumbnail;
        $thumbnail = $user->thumbnail && is_readable($thumbnailPath) ? $user->thumbnail : 'no-profile-pic.png';
        return view('dashboard.edit-profile', [
            'user' => $user,
            'thumbnail' => $thumbnail,
            'role' => $user->roles[0]->name
        ]);
    }

    public function updateProfile(EditProfileRequest $request)
    {
        $this->userRepository->updateUser($request);

        return redirect(route('edit-profile'));
    }
}
