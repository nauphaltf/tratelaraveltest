<?php

namespace App\Repositories;

use Auth;

class UserRepository
{
    public function getUser()
    {
        return Auth::user();
    }

    public function updateUser($request)
    {
        $user = Auth::user();
        $user->name = $request->get('name');

        if ($request->hasFile('thumbnail')) {
            $attachment = $request->file('thumbnail');
            $fileName = str_random(3) . '_' . $attachment->getClientOriginalName();
            $uploaded = $attachment->move('images/profile', $fileName);
            $user->thumbnail = $fileName;
        }

        $user->save();
    }
}